# Open vSwitch Lab


Mục lục
========

* [1. Cài đặt OpenvSwitch](#install)
* [2. Topo mạng lúc đầu](#topo)
* [3. Tạo một bridge](#create_br0)
* [4. Gán interface eth0 cho br0](#assign_br0)
* [5. Gán IP cho br0](#ip)
* [6. Tạo thêm 2 TAP interface cho máy physical](#create_tap)
* [7. Gán TAP interface mới cho br0](#assign_tap)
* [8. Kết nối VMs với TAP interfaces](#vm)
* [9. Topo mạng mới](#new_topo)
* [10. References](#ref)

-----------------------------------------------------------

## <a name="install">1. Cài đặt OpenvSwitch</a>

Đây mình cài OVS trên Ubuntu 14.04
```
sudo apt-get install -y openvswitch-switch openvswitch-common
```

## <a name="topo">2. Topo mạng lúc đầu</a>

Trên Ubuntu 14.04 Desktop, mình dùng VirtualBox để cài các máy ảo.

![topo](/img/block_1.png)


## <a name="create_br0">3. Tạo một bridge</a>

Ở đây mình sẽ tạo một bridge có tên là br0 (chú ý bạn phải có quyền người dùng cao nhất `sudo su`).
```
# ovs-vsctl add-br br0
# ifconfig br0 up
# ifconfig
```
![create brige](img/br0_2.png)

## <a name="assign_br0">4. Gán interface eth0 cho br0</a>

```
# ovs-vsctl add-port br0 eth0
```
Lúc này chúng ta sẽ không kết nối Internet được.

## <a name="ip">5. Gán IP cho br0</a>

- Xóa IP từ interface eth0
```
# ifconfig eth0 0
```
- Chạy DHCP discover từ br0 (để cấp IP cho br0)
```
# dhclient br0
```
Đến đây chúng ta có thể kết nối được mạng rồi, và sẽ thấy IP mới được cấp cho br0

![br0](/img/br0_3.png)


## <a name="create_tap">6. Tạo thêm 2 TAP interface cho máy physical</a>

Đến đây, IP (DHCP) của 2 máy ảo trên vẫn còn hữu hiệu nhưng không thể kết nối được với nhau cho đến khi chúng ta gán nó cho bridge mới. Để làm như vậy, ta cần tạo thêm 2 TAP interface mới (nếu ta có bao nhiêu VMs thì phải tạo thêm bấy nhiều TAP interfaces).

```
# ip tuntap add mode tap vmport1
# ip tuntap add mode tap vmport2
# ifconfig vmport1 up
# ifconfig vmport2 up
```
Vì vậy ta tạo thêm được 2 TAP interface có tên là vmport1 và vmport2.
 
![tap](/img/br0_5.png)


**Note**: Chúng ta có thể nình thấy các interface khác nhưng nó không liên quan trong việc cấu hình của mình.

## <a name="assing_tap">7. Gán TAP interface mới cho br0</a>

Vì vậy chúng ta có một br0 và 2 TAP interfaces gọi là vmport1 và vmport2. Công việc tiếp theo là gán 2 TAP này cho br0.

```
# ovs-vsctl add-port br0 vmport1
# ovs-vsctl add-port br0 vmport2
```
OK, xem xét sơ đồ dưới đây:
(Oops! interface eth0 không có IP đâu nhé!)

![TAP](/img/br0_6.png)

Ta thấy rằng 2 interface đó đã gán cho br0. Chúng ta có thể kiểm tra bằng cách chạy `ovs-vsctl show`:

![interface](/img/br0_7.png)

## <a name="vm">8. Kết nối VMs với TAP interfaces</a>

- Với VM1 (vagrant_channel)

![vm](/img/vm1.png)

- Với VM2 (vagrant_mininet)

![vm](/img/vm2.png)

## <a name="new_topo">9. Topo mạng mới</a>

(Oops! interface eth0 không có IP đâu nhé!)

![ovs](/img/br0_8.png)

Bây giờ VM1 và VM2 có thể truy cập được Internet và ping lẫn nhau rồi.


**Note**: Việc cấu hình này sẽ mất sau khi khởi động lại.

Đây là script để tải toàn bộ việc cấu hình sau khi khởi động:

```
#!/bin/bash
#### OVS boot parameters ####

echo “Loading vPorts”

# Load as many virtual interfaces as you have

ip tuntap add mode tap vmport1
ip tuntap add mode tap vmport2
ifconfig vmport1 up
ifconfig vmport2 up
echo “Deleting IP address from eth0”
ifconfig eth0 0
sleep 2
echo “Starting br0”
ifconfig br0 up
sleep 2
dhclient br0
echo “Restarting Open vSwitch…”

# It is important to restart the service to load the interfaces into the OVS configuration.
service openvswitch-switch restart
echo “Restarting Open vSwitch [OK]”

```

* Lưu file: /etc/ovs-persistent.sh
* Thay đổi quyền thực thi: chmod +x /etc/ovs-persistent.sh
* Chỉnh sửa file: /etc/rc.local và thêm dòng: sh /etc/ovs-persistent.sh

-------------------------------------
Thanks for your reading! ^-^

## <a name="ref">10. References</a>

https://www.evoila.de/2015/11/02/open-vswitch-basics/?lang=en
